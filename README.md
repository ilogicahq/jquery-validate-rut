# jquery-validate-rut

This is a jQuery-validate plugin for fields of type `rut` (National
identification number for Chile)

It checks that the last number (optionally separated by a dash) corresponds
to the modulus 11 (mod 11) of the rest of the numbers.

## Requirements

This plugins depends on:

- jQuery
- jQuery-validate

## Ussage

This plugin registers a `rut` validator that can be attached to any input
of type text.

In this example, we se up a `input` type `text` with a name of `rutInput` as
required, with max length of 15 and with the `rut` validator. It also sets
a message to display when the validation fails.


``` javascript
$('form').validate(function() {
  rules:{
    'rutInput': {
      required: true,
      rut: true,
      maxlength: 15}
  },
  messages: {
    'rut': {
       required: "Ingresa tu RUT",
       rut: "Ingresa un RUT válido",
       maxlength: "Excedes el límite de caracteres"
    }
  },
});
```

## Contributing

### Development Requirements

You are going to need the folowing installed on the dev machine to build:
- NodeJs
- Gulp

### Cloning

After cloning the repo, execute the following commands:

``` sh
$ npm install
$ gulp install
```

### Building

Plase build the project before any commit. This will keep the .min file
in sync with the plain js.

To build, just execute:


``` sh
$ gulp
```
