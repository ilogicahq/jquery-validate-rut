var gulp = require('gulp');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');

gulp.task('minify', function () {
   gulp.src([
        'jquery-validate-rut.js'
      ])
      .pipe(uglify())
      .pipe(rename('jquery-validate-rut.min.js'))
      .pipe(gulp.dest('.'));
});

gulp.task('default', ['minify']);
